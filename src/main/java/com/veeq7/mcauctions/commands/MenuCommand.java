package com.veeq7.mcauctions.commands;

import com.veeq7.mcauctions.utils.ArgsStack;
import com.veeq7.mcauctions.utils.McCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class MenuCommand extends McCommand {
    public MenuCommand() {
        super("menu", "Shows Auctions menu");
    }

    @Override
    protected boolean execute(CommandSender sender, ArgsStack args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "You must be a player to use this command!");
            return false; // TODO: probably return true to not display usage message?
        }

        Player player = (Player)sender;
        Inventory inventory = Bukkit.createInventory(player, 27, "Menu");
        ItemStack stone = new ItemStack(Material.STONE);

        for (int i = 0; i < 27; i++) {
            inventory.setItem(i, stone);
        }
        player.openInventory(inventory);

        return true;
    }

    @Override
    protected List<String> getTabCompletions(CommandSender sender, ArgsStack args) {
        return new ArrayList<>();
    }
}
