package com.veeq7.mcauctions.commands;

import com.veeq7.mcauctions.utils.ArgsStack;
import com.veeq7.mcauctions.utils.McCommand;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public class TestCommand extends McCommand {
    public TestCommand() {
        super("test", "Test command");
    }

    @Override
    public boolean execute(CommandSender sender, ArgsStack args) {
        sender.sendMessage("This is a test command");
        return true;
    }

    @Override
    public List<String> getTabCompletions(CommandSender sender, ArgsStack args) {
        return new ArrayList<>();
    }
}
