package com.veeq7.mcauctions.utils;

import org.bukkit.command.*;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public abstract class McCommand implements CommandExecutor, TabCompleter {
    protected String name;
    protected String description;
    protected final List<String> aliases = new ArrayList<>();

    protected McCommand(String name, String description) {
        this.name = name.toLowerCase(); // we depend on lowercase for parsing and hash maps
        this.description = description;
    }

    public McCommand addAlias(String alias) {
        aliases.add(alias);
        return this;
    }

    @Override
    public final boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return execute(sender, new ArgsStack(label, args));
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        return getTabCompletions(sender, new ArgsStack(label, args));
    }

    protected abstract boolean execute(CommandSender sender, ArgsStack args);

    protected abstract List<String> getTabCompletions(CommandSender sender, ArgsStack args);

    public static void register(JavaPlugin plugin, McCommand command) {
        Logger logger = plugin.getLogger();
        logger.info("Registering command " + command.name + "...");

        PluginCommand bukkitCommand = plugin.getCommand(command.name);
        if (bukkitCommand == null) {
            logger.severe("No command named " + command.name + " in plugin.yml!");
            return;
        }

        bukkitCommand.setName(command.name);
        bukkitCommand.setDescription(command.description);
        bukkitCommand.setAliases(command.aliases);
        bukkitCommand.setExecutor(command);
        bukkitCommand.setTabCompleter(command);
    }
}
