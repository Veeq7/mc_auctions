package com.veeq7.mcauctions.utils;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

public final class McNamespace extends McCommand {
    private static final String HELP_COMMAND = "help";
    private final HashMap<String, McCommand> full_commands = new HashMap<>(); // mostly for help menu
    private final HashMap<String, McCommand> commands_and_aliases = new HashMap<>(); // for calling and tab completions

    public McNamespace(String name, String description) {
        super(name, description);
    }

    public McNamespace add(McCommand command) {
        if (full_commands.containsKey(command.name) || command.name.equals(HELP_COMMAND)) {
            // TODO: log invalid subcommand?
            return this;
        }
        full_commands.put(command.name, command);
        commands_and_aliases.put(command.name, command);
        for (String alias : command.aliases) {
            commands_and_aliases.put(alias, command);
        }
        return this;
    }

    public McNamespace addSubNamespace(String name, String description) {
        McNamespace namespace = new McNamespace(name, description);
        add(namespace);
        return namespace;
    }

    public McNamespace addAll(McNamespace namespace) {
        for (McCommand command : namespace.full_commands.values()) {
            full_commands.putIfAbsent(command.name, command);
        }
        for (Map.Entry<String, McCommand> entry : namespace.commands_and_aliases.entrySet()) {
            commands_and_aliases.putIfAbsent(entry.getKey(), entry.getValue());
        }
        return this;
    }

    private List<McCommand> getCommandsSorted() {
        return full_commands
                .values()
                .stream()
                .sorted(Comparator.comparingInt(a -> (a instanceof McNamespace ? 0 : 1)))
                .toList();
    }

    @Override
    protected boolean execute(CommandSender sender, ArgsStack args) {
        String base = args.parsedAsString();
        String arg = args.pop().orElse(HELP_COMMAND);
        if (arg.equals(HELP_COMMAND)) {
            for (McCommand command : getCommandsSorted()) {
                sender.sendMessage(ChatColor.GOLD + "/" + base + command.name + ": " + ChatColor.RESET + command.description);
            }
            sender.sendMessage(ChatColor.GOLD + "/" + base + "help: " + ChatColor.RESET + "Shows this help menu");
            sender.sendMessage(ChatColor.GOLD + "/" + base + "aliases: " + ChatColor.RESET + "Shows aliases for this namespace");
            return true;
        }

        McCommand command = commands_and_aliases.get(arg);
        if (command == null) {
            sender.sendMessage(ChatColor.RED + "Command \"" + ChatColor.YELLOW + args.parsedAsString() + ChatColor.RED + "\" does not exist!");
            return false;
        }
        return command.execute(sender, args);
    }

    @Override
    protected List<String> getTabCompletions(CommandSender sender, ArgsStack args) {
        Optional<String> arg = args.pop();
        if (arg.isPresent()) {
            String argString = arg.get();
            if (argString.equals(HELP_COMMAND)) {
                return new ArrayList<>();
            }
            McCommand command = commands_and_aliases.get(argString);
            if (command != null) {
                return command.getTabCompletions(sender, args);
            }
        }

        List<String> result = new ArrayList<>(commands_and_aliases.keySet());
        result.add(HELP_COMMAND);
        return result;
    }

    public static McNamespace makeRoot(JavaPlugin plugin, String name, String description) {
        McNamespace root = new McNamespace(name, description);
        register(plugin, root);
        return root;
    }
}
