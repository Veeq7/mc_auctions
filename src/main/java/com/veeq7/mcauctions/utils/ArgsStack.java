package com.veeq7.mcauctions.utils;

import org.bukkit.Material;

import java.util.Optional;

public class ArgsStack {
    private int index;
    private final String label;
    private final String[] args;

    ArgsStack(String label, String[] args) {
        this.index = 0;
        this.label = label;
        this.args = args;
    }

    public Optional<String> pop() {
        if (index < args.length) {
            return Optional.of(args[index++].toLowerCase());
        }
        return Optional.empty();
    }

    public Optional<Material> popMaterial() {
        Optional<String> arg = pop();
        if (arg.isPresent()) {
            Material material = Material.getMaterial(arg.get());
            if (material != null) {
                return Optional.of(material);
            }
        }
        return Optional.empty();
    }

    public String parsedAsString() {
        StringBuilder builder = new StringBuilder();
        builder.append(label);
        builder.append(' ');
        for (int i = 0; i < index; i++) {
            builder.append(args[i]);
            builder.append(' ');
        }
        return builder.toString();
    }
}
