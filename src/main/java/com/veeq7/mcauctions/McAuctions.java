package com.veeq7.mcauctions;

import com.veeq7.mcauctions.commands.MenuCommand;
import com.veeq7.mcauctions.commands.TestCommand;
import com.veeq7.mcauctions.utils.McNamespace;
import org.bukkit.plugin.java.JavaPlugin;

public class McAuctions extends JavaPlugin {
    private static McAuctions theInstance;

    public static McAuctions instance() {
        return theInstance;
    }

    @Override
    public void onEnable() {
        theInstance = instance();

        McNamespace rootNamespace = McNamespace.makeRoot(this, "mcauctions", "The best auctions plugin ever made!");
        rootNamespace.add(new TestCommand());

        McNamespace auctionNamespace = rootNamespace.addSubNamespace("auction", "Auctions stuff...");
        auctionNamespace.add(new TestCommand());
        auctionNamespace.add(new MenuCommand());

        McNamespace testNamespace = rootNamespace.addSubNamespace("namespace", "Test namespace");
        testNamespace.addAlias("name");
        testNamespace.add(new TestCommand());
    }
}
